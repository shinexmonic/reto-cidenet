import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    meta: { requiresAuth: false },
    component: () => import("../components/Login"),
  },
  {
    path: '/home',
    meta: { requiresAuth: true },
    name: 'home',
    component: HomeView,
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach(async (to, from, next) => {
  const userLocal = JSON.parse(window.localStorage.getItem("authUser") || "{}");
  if (!to.matched.some((route) => route.meta.requiresAuth)){
    next();return;
  }

  if (to.matched.some((route) => route.meta.requiresAuth) && Object.keys(userLocal).length > 0) 
  {
    next();return
  } 
  else
  {
    window.location.href = "/login";
  }
  
});


export default router
