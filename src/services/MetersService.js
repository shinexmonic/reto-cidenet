import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://ops.enerbit.dev/learning/api/v1",
  headers: {
    Accept: "applicacion/json",
    "Content-Type": "application/json",
  },
});

// Exportamos los métodos para las peticiones
export default {
  getItems(url, options) {
    return apiClient.get(`/${url}`, {params: options});
  },
  getItemEdit(url, id) {
    return apiClient.get(`/${url}/${id}`,);
  },
  updateItem(url, id, body) {
    return apiClient.patch(`/${url}/${id}`, body);
  },
  deleteItemId(url, id){
    return apiClient.delete(`/${url}/${id}`);
  },
  createItem(url, body){
    return apiClient.post(`/${url}`, body);
  }
};